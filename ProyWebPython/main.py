

# importamos flask
from flask import Flask, render_template, url_for, redirect

# creamos la aplicación teniendo en cuenta una variable
app = Flask(__name__)

# crear las rutas del fichero
@app.route('/')  # / es la ruta principal del proyecto
# asigno la ruta a una función
def index():
    # return "<br><br><br><h1>Aprendiendo Web con Flask y Python...</h1>"
    return render_template('index.html')

@app.route('/uniminuto')
def uniminuto():
    # return "<br><br><br><h1>Uniminuto...</h1>"
    return render_template('uniminuto.html')

@app.route('/estudiantes')
def estudiantes():
    # return "<br><br><br><h1>Estudiantes Uniminuto...</h1>"
    return render_template('estudiantes.html')

# identificar el fichero principal
if __name__ == '__main__' :
    app.run(debug = True)   # reinicia el servidor automáticamente